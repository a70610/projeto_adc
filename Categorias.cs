﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Classe <c> Categorias </c> é uma Classe utilizada fazer a seleção de categorias e através de botões ativar os respetivos canvas.
/// </summary>
public class Categorias : MonoBehaviour
{
    /// <summary>
    /// Texto é uma variável alterada durante o decorrer da app para dizer se estamos nos destaques ou categorias.
    /// </summary>
    [Header("Variaveis de Botoes dos Primeiros Canvas")]
    public Text texto;
    /// <summary>
    /// ScroolView é utilizado para mostrar todos os veiculos de cada categoria ou destaques.
    /// </summary>
    public GameObject ScroollView;
    /// <summary>
    /// Canvas com os botões de categorias.
    /// </summary>
    public GameObject Botoes_Categoria;
    /// <summary>
    /// Canvas da Categoria de Carros.
    /// </summary>
    [Header("Variavel do Canvas")]
    public GameObject Carros_Canvas;
    /// <summary>
    /// Canvas da Categoria de Motas.
    /// </summary>
    public GameObject Motas_Canvas;
    /// <summary>
    /// Canvas da Categoria de Contrabando.
    /// </summary>
    public GameObject ContraBando_Canvas;
    /// <summary>
    /// Canvas com os botões de categorias.
    /// </summary>
    public GameObject Botoes;
    /// <summary>
    /// Botão de Voltar.
    /// </summary>
    public GameObject Botao_voltar;
    /// <summary>
    /// DropDown com as opções de ver Destques ou Categorias.
    /// </summary>
    public GameObject DropDown;
    /// <summary>
    /// ScroolView é utilizado para mostrar todos os veiculos da categoria de carros.
    /// </summary>
    [Header("Variaveis para a Categoria Carros")]
    public GameObject ScroolView_Carros;
    /// <summary>
    /// ScroolView é utilizado para mostrar todos os veiculos da categoria de motas.
    /// </summary>
    [Header("Variaveis para a Categoria Motas")]
    public GameObject ScroolView_Motas;
    /// <summary>
    /// ScroolView é utilizado para mostrar todos os veiculos da categoria de contrabando.
    /// </summary>
    [Header("Variaveis para a Categoria ContraBando")]
    public GameObject ScroolView_Contrabando;

    /// <summary>
    ///     <b><i>Função Start.</i></b>
    ///     <para>Função Utilizada no iniciar da aplicação para <b><u>desativar o canvas dos botões</u></b> do canvas de categorias.</para>
    /// </summary>
    /// <example>
    ///     <b>Propriedades:</b>
    ///     <value>
    ///         <code>
    ///             Botoes_Categoria.SetActive(false);
    ///         </code>
    ///     </value>
    /// </example>
    private void Start()
    {
        Botoes_Categoria.SetActive(false);
    }
    /// <summary>
    ///     <b><i>Categoriases the change.</i></b>
    /// </summary>
    /// <param name="opcao">Valor inteiro usado para fazer verificações.</param>
    /// Botão que recebe o valor para que saiba se deve mostrar os destaques ou as categorias.
    /// <example>
    ///     <b>Propriedades:</b>
    ///     <value>
    ///         <code>
    ///             texto.text = "Destaques";
    ///             ScroollView.SetActive(true);
    ///             Botoes_Categoria.SetActive(false);
    ///         </code>
    ///         Depois de receber o valor, faz a verificação, caso seja <b>0</b>, mostra o canvas dos destaques e caso seja <b>1</b> mostra o das categorias.
    ///     </value>
    /// </example>
    public void Categorias_Change(int opcao)
    {
        if(opcao == 0)
        {
            texto.text = "Destaques";
            ScroollView.SetActive(true);
            Botoes_Categoria.SetActive(false);
        }
        else
        {
            texto.text = "Categorias";
            ScroollView.SetActive(false);
            Botoes_Categoria.SetActive(true);
        }
    }
    /// <summary>
    /// <b><i>Botão Voltar.</i></b>
    /// <para>Botão utilizado para <b><u>voltar de cada categoria</u></b> para o menu de categorias.</para>
    /// </summary>
    /// <example>
    ///     <b>Propriedades:</b>
    ///     <code>
    ///         Botao_voltar.SetActive(false);
    ///         DropDown.SetActive(true);
    ///         Botoes.SetActive(true);
    ///         texto.text = "Categorias";
    ///     </code>
    ///     O código acima serve para desativar o botão <b><u>voltar do canvas de cada categoria</u></b> dependedo da que tenha selecionada, e <b>ativa</b> o canvas da categoria mudando assim o texto para Categoria.
    /// </example>
    public void Voltar()
    {
        if(Carros_Canvas.active)
        {
            Carros_Canvas.SetActive(false);
        }
        if (Motas_Canvas.active)
        {
            Motas_Canvas.SetActive(false);
        }
        if (ContraBando_Canvas.active)
        {
            ContraBando_Canvas.SetActive(false);
        }
        Botao_voltar.SetActive(false);
        DropDown.SetActive(true);
        Botoes.SetActive(true);
        texto.text = "Categorias";
    }
    /// <summary>
    /// <b>Ativação da categoria de carros.</b>
    /// <para>Esta função é usada para <b><u>todos os carros</u></b> que o stand vende.</para>
    /// </summary>
    /// <example>
    ///     <b>Propriedades:</b>
    ///     <value>
    ///         <code>
    ///             ContraBando_Canvas.SetActive(false);
    ///             Motas_Canvas.SetActive(false);
    ///             Carros_Canvas.SetActive(true);
    ///         </code>
    ///         No código acima podemos ver como <b><u>desativar todas as categorias</u></b> exceto a que queremos mostrar, neste caso a de carros.
    ///     </value>
    /// </example>
    public void Categoria_Carros()
    {
        Botao_voltar.SetActive(true);
        DropDown.SetActive(false);
        texto.text = "Carros";
        Botoes.SetActive(false);
        Carros_Canvas.SetActive(true);
        ScroolView_Carros.SetActive(true);

        Motas_Canvas.SetActive(false);
        ContraBando_Canvas.SetActive(false);
    }
    /// <summary>
    /// <b>Ativação da categoria de motas.</b>
    /// <para>Esta função é usada para <b><u>todas as motas</u></b> que o stand vende.</para>
    /// </summary>
    /// <example>
    ///     <b>Propriedades:</b>
    ///     <value>
    ///         <code>
    ///             ContraBando_Canvas.SetActive(false);
    ///             Motas_Canvas.SetActive(true);
    ///             Carros_Canvas.SetActive(false);
    ///         </code>
    ///         No código acima podemos ver como <b><u>desativar todas as categorias</u></b> exceto a que queremos mostrar, neste caso a de motas.
    ///     </value>
    /// </example>
    public void Categoria_Motas()
    {
        Botao_voltar.SetActive(true);
        DropDown.SetActive(false);
        texto.text = "Motas";
        Botoes.SetActive(false);
        Motas_Canvas.SetActive(true);
        ScroolView_Motas.SetActive(true);

        Carros_Canvas.SetActive(false);
        ContraBando_Canvas.SetActive(false);
    }
    /// <summary>
    /// <b>Ativação da categoria de contrabando.</b>
    /// <para>Esta função é usada para <b><u>todos os veiculos de contrabando</u></b> que o stand vende.</para>
    /// </summary>
    /// <example>
    ///     <b>Propriedades:</b>
    ///     <value>
    ///         <code>
    ///             ContraBando_Canvas.SetActive(true);
    ///             Motas_Canvas.SetActive(false);
    ///             Carros_Canvas.SetActive(false);
    ///         </code>
    ///         No código acima podemos ver como <b><u>desativar todas as categorias</u></b> exceto a que queremos mostrar, neste caso a de contrabando.
    ///     </value>
    /// </example>
    public void Categoria_Contrabando()
    {
        Botao_voltar.SetActive(true);
        DropDown.SetActive(false);
        texto.text = "Contrabando";
        Botoes.SetActive(false);
        ContraBando_Canvas.SetActive(true);
        ScroolView_Contrabando.SetActive(true);

        Motas_Canvas.SetActive(false);
        Carros_Canvas.SetActive(false);
    }
}
