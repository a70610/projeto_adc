﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

/// <summary>
/// Classe <c> Carrinho </c> é uma Classe utilizada para adicionar veiculos selecionados ao carrinho
/// </summary>
public class Carrinho : MonoBehaviour
{
    /// <summary>
    /// Canvas Dos Destaques mostrado no Ecra no inicio da app
    /// </summary>
    public GameObject Default_Canvas;
    /// <summary>
    /// Canvas do Carrinho de Compras
    /// </summary>
    public GameObject Carrinho_Canvas;
    /// <summary>
    /// Canvas da Categoria de carros, nele apenas são apresentados carros
    /// </summary>
    public GameObject Carros;
    /// <summary>
    /// Canvas da Categoria de Motas, nela apenas são mostradas motas
    /// </summary>
    public GameObject Motas;
    /// <summary>
    /// Canvas da Categoria de Contrabando, nela apenas são mostrados veiculos desta categoria
    /// </summary>
    public GameObject Contrabando;
    /// <summary>
    /// Objeto da applicacao onde são inseridas as imagens, texto, etc, de todos os veiculos
    /// </summary>
    public GameObject content;
    /// <summary>
    /// Fonte Utilizada em todo o texto
    /// </summary>
    public Font Roboto;
    /// <summary>
    /// Variavel de texto presente no Canvas do Carrinho de Compras
    /// </summary>
    [Header("Canvas do Carrinho de Compras")]
    [Space(3)]
    public Text Nome_Text;
    /// <summary>
    /// Variavel de texto presente no Canvas do Carrinho de Compras
    /// </summary>
    public Text Total_Preco;
    /// <summary>
    /// Botao para finalizar as compras presente no Canvas do Carrinho de Compras
    /// </summary>
    public GameObject Fim_Compras;
    /// <summary>
    /// Variavel de control do caminho de pastas, onde têm que ir buscar as informacoes a cerca dos veiculos
    /// </summary>
    string carros_path = "Stand/Carros/";
    /// <summary>
    /// Variavel de control do caminho de pastas, onde têm que ir buscar as informacoes a cerca dos veiculos
    /// </summary>
    string motas_path = "Stand/Motas/";
    /// <summary>
    /// Variavel de control do caminho de pastas, onde têm que ir buscar as informacoes a cerca dos veiculos
    /// </summary>
    string contrabando_path = "Stand/Contrabando/";

    /// <summary>
    ///     <i><b>Abrir carrinho de compras.</b></i>
    ///    <para>Função utilizada para <b>abrir o carrinho de compras</b>.</para>
    /// </summary>
    /// <value> Propriedade:
    /// <para>Através do uso das variáveis presentes, conseguimos controlar os Canvas que pretendemos mostrar e os que não queremos.</para>
    ///     <example>
    ///         <b>Por Exemplo: </b>
    ///             <code>
    ///                 Carrinho_Canvas.SetActive(true);
    ///             </code>
    ///         Serve para ativar o Canvas do <b>carrinho de compras</b> mas caso:
    ///         <code>
    ///             Carrinho_Canvas.SetActive(false);
    ///         </code>
    ///         Desativava o Canvas do <b>carrinho de compras</b>
    ///     </example>
    ///     <example>
    ///         <code>
    ///             Default_Canvas.SetActive(false);
    ///         </code>
    ///     </example>
    ///     <C><b>Default_Canvas</b></C> Torna o objeto presente na scene oculto
    ///     <example>
    ///         <code>
    ///             Carros.SetActive(false);
    ///             Motas.SetActive(false);
    ///             Contrabando.SetActive(false);
    ///         </code>
    ///     </example>
    ///     O Código anterior serve para <b>desativar</b> todos os <b>Canvas</b> que eles representam
    /// </value>
    public void Abrir_Carrinho()
    {
        Carrinho_Canvas.SetActive(true);
        Default_Canvas.SetActive(false);
        Carros.SetActive(false);
        Motas.SetActive(false);
        Contrabando.SetActive(false);
    }
    /// <summary>
    ///     <b><i>Fechar carrinho de compras.</b></i>
    ///     <para>Função utilizada para fazer o <b>controlo dos Canvas</b>.</para>
    /// </summary>
    /// <value>
    ///     <b>Propriedade:</b>
    ///     <para>Através do uso das variáveis presentes, conseguimos controlar os Canvas que pretendemos mostrar e os que não queremos.</para>
    ///         <para>
    ///             <example>
    ///                 <code>
    ///                     Carrinho_Canvas.SetActive(false);
    ///                     Default_Canvas.SetActive(true);
    ///                 </code>
    ///                 O código anteriro desativa o <b>Canvas do carrinho de compras </b> e volta apresentar o <b>Canvas dos veiculos em destaque</b>
    ///             </example>
    ///         </para>
    /// </value>
    public void Fechar_Carrinho()
    {
        Carrinho_Canvas.SetActive(false);
        Default_Canvas.SetActive(true);
    }
    /// <summary>
    /// <b><i>Adicona ao carrinho de compras um Veiculo.</b></i>
    /// </summary>
    /// <value>
    ///     <b>Propriedade:</b>
    ///     <para>Função utilizada para <b>receber</b> a informação da Função <see cref="verif_Veiculo_Pasta"/> de modo a saber em que <b>diretoria se encontram</b> os ficheiros.</para>
    ///     <para>Em seguida chama <see cref="Veiculo_add(string)"/> para adicionar um veiculo com o caminho verificado anteriormente.</para>
    ///     <para>
    ///         <example>
    ///             <code>
    ///                 string caminho_Verificado = "";
    ///                 caminho_Verificado =  verif_Veiculo_Pasta();
    ///             </code>
    ///             Primeiramente, <b><u>inicializamos a variável</u></b>, em seguida chamamos a função <see cref="verif_Veiculo_Pasta"/> de modo a verificar o caminho e assim <b>atribuir à variável</b> o caminho retornado pela função.
    ///             <para>Em Seguida:</para>
    ///             <para>
    ///                 <code>
    ///                     if (caminho_Verificado == carros_path)
    ///                     {
    ///                         Veiculo_add(caminho_Verificado);
    ///                     }
    ///                 </code>
    ///                 Assim como a condição de exemplo enteriormente mostrada, conseguimos perceber que o passo seguinte é <b><u>adicionar um veiculo</u></b> chamando a função <see cref="Veiculo_add(string)"/>
    ///                 enviando o caminho anteriormente recebido.
    ///             </para>
    ///         </example>
    ///     </para>
    /// </value>
    public void carrinho_add()
    {
        string caminho_Verificado = "";

        caminho_Verificado =  verif_Veiculo_Pasta();
        Debug.Log(caminho_Verificado);
        DirectoryInfo Veiculo = new DirectoryInfo(Application.dataPath + "/Resources/" + caminho_Verificado + Nome_Text.text.ToUpper());
        FileInfo[] IMG_Veiculo = Veiculo.GetFiles();

        if (caminho_Verificado == carros_path)
        {
            Veiculo_add(caminho_Verificado);
        }
        else if(caminho_Verificado == motas_path)
        {
            Veiculo_add(caminho_Verificado);
        }
        else if(caminho_Verificado == contrabando_path)
        {
            Veiculo_add(caminho_Verificado);
        }
        
    }
    /// <summary>
    /// <b><i>Adição de Veiculos ao carrinho de compras.</i></b>
    /// </summary>
    /// <example>
    ///     Esta função é usada para <b>adicionar todos os veiculos</b> à scene, <u>Imagem</u>, <u>Preço</u> e <u>Nome</u>.
    ///     <para>Alteração de Imagem: </para>
    ///     <code>
    ///         var currentSprite = Resources.Load<Sprite>(caminho_Verificado + Nome_Text.text.ToUpper() + "/1");
    ///         Image NewImage = NewObj.AddComponent<Image>();
    ///         NewImage.sprite = currentSprite;
    ///     </code>
    ///     No Exemplo anterior, podemos ver como é feita uma troca de imagem e como se vai buscar ao diretório.
    ///     <para>
    ///         Através do <b><u>Resources.Load<Sprite>(String Caminho)</u></b> conseguimos ir buscar a imagem ao seu diretório.
    ///     </para>
    /// </example>
    /// <param name="caminho_Verificado">Recebe o Caminho que é usado para definir o diretório onde se têm que ir buscar.
    /// <para>O caminho vêm da função <see cref="carrinho_add"/>.</para></param>
    /// <example>
    ///     Alteração de Texto:
    ///     <code>
    ///         Nome.text = Nome_Text.text;
    ///         Nome.font = Roboto;
    ///         Nome.fontSize = 100;
    ///     </code>
    ///     No código acima, podemos ver como <b><u>alterar o texto</u></b> e configurá-lo, mudando a fonte e o seu tamanho.
    ///     <para>Na <b>alteração/adição</b> do preço ao veiculo, são usados <b>ficheiros</b> onde em cada um, na sua última linha se encontra o <b>preço</b></para>
    ///     <para>Ainda temos uma verificação a mais devido a um dos veiculos onde nele existem letras no preço e as mesmas precisam ser removidas</para>
    /// </example>
    private void Veiculo_add(string caminho_Verificado)
    {
        //adicionar imagem
        var currentSprite = Resources.Load<Sprite>(caminho_Verificado + Nome_Text.text.ToUpper() + "/1");
        GameObject NewObj = new GameObject(); //Cria um novo GameObject na Scene
        Image NewImage = NewObj.AddComponent<Image>(); //Adiciona o componente Image ao NewObj
        NewObj.name = Nome_Text.text; //Altera o nome do GameObject para o mesmo que o sprite tiver
        NewObj.GetComponent<RectTransform>().SetParent(content.transform); //Faz com que o NewObj seja "Filho" do content
        NewObj.GetComponent<RectTransform>().sizeDelta = new Vector2(500, 500);
        NewObj.GetComponent<RectTransform>().localScale = new Vector2(1, 1);
        NewObj.SetActive(true);
        NewImage.sprite = currentSprite;
        //fim da adicao da imagem

        //adicionar nome
        GameObject Texto_Obj = new GameObject();
        Text Nome = Texto_Obj.AddComponent<Text>();
        Nome.text = Nome_Text.text;
        Texto_Obj.GetComponent<RectTransform>().SetParent(content.transform);
        Texto_Obj.GetComponent<RectTransform>().localScale = new Vector2(1, 1);
        //Configuracao do Nome
        Nome.font = Roboto;
        Nome.fontSize = 100;
        Nome.horizontalOverflow = HorizontalWrapMode.Overflow; //Modifica a propriedade horizontal do text
        Nome.alignment = TextAnchor.MiddleCenter; //Modifica o alinhamento do text
        //Fim da adicao do nome

        //adicionar preco
        string file_Location = Application.dataPath + "/Resources/" + caminho_Verificado + Nome_Text.text.ToUpper() + "/Description.txt";
        string[] ALLLINES = System.IO.File.ReadAllLines(file_Location);
        long count = 0;
        using (StreamReader r = new StreamReader(file_Location))
        {
            string line;
            while ((line = r.ReadLine()) != null)
            {
                count++;
            }
        }
        string file_Preco = ALLLINES[count - 1];

        GameObject Preco = new GameObject();
        Text Preco_texto = Preco.AddComponent<Text>();
        Preco.GetComponent<RectTransform>().SetParent(content.transform);
        Preco.GetComponent<RectTransform>().localScale = new Vector2(1, 1);
        Preco_texto.font = Roboto;
        Preco_texto.fontSize = 100;
        Preco_texto.horizontalOverflow = HorizontalWrapMode.Overflow;
        Preco_texto.alignment = TextAnchor.MiddleCenter;
        //se for bat carocha
        if (Nome_Text.text == "Bat Carocha")
        {
            file_Preco = file_Preco.Remove(0, 21);
        }
        //fim do bat carocha
        Preco_texto.text = file_Preco + "€";
        //fim da adicao de preco

        //Alterar Preco
        string temp_Preco = Total_Preco.text.Substring(0, Total_Preco.text.Length - 1);
        file_Preco = file_Preco.Replace(".", string.Empty);
        file_Preco = file_Preco.Replace(",", string.Empty);
        file_Preco = file_Preco.Replace("€", string.Empty);
        temp_Preco = (float.Parse(temp_Preco) + float.Parse(file_Preco)).ToString();
        Total_Preco.text = temp_Preco + " €";
        //Fim da Alteracao de Preco
    }
    /// <summary>
    ///     <i><b>Verificação da Existencia de Veiculos na Pasta.</b></i>
    ///     <para>Esta função serve para fazer a verificação da existência de veiculos através das suas dirétorias.</para>
    /// </summary>
    /// <example>
    ///     <code>
    ///         System.IO.Directory.Exists(Application.dataPath + "/Resources/" + carros_path + Nome_Text.text.ToUpper())
    ///     </code>
    ///     Com a linha de código acima conseguimos <b>verificar a existência do diretório</b> nesse caso de um carro, cada <b><u>carro/veiculo têm a sua própria dirétoria</u></b> e nela a informação que necessita.
    /// </example>
    /// <returns>Retorna o Caminho do Veiculo caso exista em alguma dirétoria</returns>
    private string verif_Veiculo_Pasta()
    {
        if(System.IO.Directory.Exists(Application.dataPath + "/Resources/" + carros_path + Nome_Text.text.ToUpper()))
        {
            DirectoryInfo Carros = new DirectoryInfo(Application.dataPath + "/Resources/" + carros_path + Nome_Text.text.ToUpper());
            FileInfo[] IMG_Carros = Carros.GetFiles();
            if (IMG_Carros.Length == 0)
            {
                Debug.Log("Nao existem carros nessa diretoria");
            }
            else
            {
                return carros_path;
            }
        }
        else if(System.IO.Directory.Exists(Application.dataPath + "/Resources/" + motas_path + Nome_Text.text))
        {
            DirectoryInfo Motas = new DirectoryInfo(Application.dataPath + "/Resources/" + motas_path + Nome_Text.text);
            FileInfo[] IMG_Motas = Motas.GetFiles();
            if (IMG_Motas.Length == 0)
            {
                Debug.Log("Nao existem Motas nessa diretoria");
            }
            else
            {
                return motas_path;
            }
        }
        else if(System.IO.Directory.Exists(Application.dataPath + "/Resources/" + contrabando_path + Nome_Text.text.ToUpper()))
        {
            DirectoryInfo Contrabando = new DirectoryInfo(Application.dataPath + "/Resources/" + contrabando_path + Nome_Text.text.ToUpper());
            FileInfo[] IMG_Contrabando = Contrabando.GetFiles();
            if (IMG_Contrabando.Length == 0)
            {
                Debug.Log("Nao existe Contrabando nessa diretoria");
            }
            else
            {
                return contrabando_path;
            }
        }

        return "Nenhum Veiculo encontrado";
    }
    /// <summary>
    ///     <b><i>Botão de Finalização da Compra.</i></b>
    ///     <para>Esta função é utilizada para finalizar uma compra, sendo ela apenas de ilutração, apenas mostra uma mensagem de agradecimento e remove todos o veiculos da lista</para>
    /// </summary>
    /// <example>
    ///     <code>
    ///         foreach (Transform child in content.transform)
    ///         {
    ///             Destroy(child);
    ///         }
    ///         Fim_Compras.SetActive(true);
    ///     </code>
    ///     O código anterior, apresenta como são <b><u>retirados os veiculos</u></b> do carrinho de compra e como é mostrada a imagem.
    /// </example>
    public void Finalizar_Compra()
    {
        foreach (Transform child in content.transform)
        {
            Destroy(child);
        }
        Fim_Compras.SetActive(true);
    }
}
