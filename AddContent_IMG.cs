﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
/// <summary>
/// Classe <c> AddContent_IMG </c> é uma Classe utilizada para adicionar conteudo aos ScroolViews presentes nas categorias e Destaques.
/// </summary>
public class AddContent_IMG : MonoBehaviour
{
    /// <summary>
    /// Objeto onde todo o conteudo será adicionado.
    /// </summary>
    public GameObject content; //Cria uma variavel do tipo GameObject    
    /// <summary>
    /// Fonto utilizada para o texto/titulos.
    /// </summary>
    public Font Roboto; //Cria uma variavel para inserires    
    /// <summary>
    /// Caminho para a pasta onde se encontram os carros.
    /// </summary>
    string carros_path = "Lista/Carros";
    /// <summary>
    /// Caminho para a pasta onde se encontram as motas.
    /// </summary>
    string motas_path = "Lista/Motas";
    /// <summary>
    /// Caminho para a pasta onde se encontram o contrabando.
    /// </summary>
    string contrabando_path = "Lista/Contrabando";
    /// <summary>
    /// Caminho para a pasta onde se encontram os carros.
    /// </summary>
    string lista_path = "Lista";
    /// <summary>
    /// Variável usada para verificar em qual das categorias se encontra a script neste caso na Lista (por defeito são os destaques)
    /// </summary>
    public bool Lista = false;
    /// <summary>
    /// Variável usada para verificar em qual das categorias se encontra a script neste caso na categoria de carros.
    /// </summary>
    public bool Carros = false;
    /// <summary>
    /// Variável usada para verificar em qual das categorias se encontra a script neste caso na categoria de motas.
    /// </summary>
    public bool Motas = false;
    /// <summary>
    /// Variável usada para verificar em qual das categorias se encontra a script neste caso na categoria de contrabando.
    /// </summary>
    public bool Contrabando = false;
    /// <summary>
    /// <b>Função Start.</b>
    /// <para>Função usada no inicio da aplicação para fazer a <b><u>verificação de que categoria</u></b> deve inserir os veiculos.</para>
    /// </summary>
    /// <example>
    ///     <b>Propriedades:</b>
    ///     <value>
    ///         <code>
    ///             if(Lista == true)
    ///             {
    ///                 CarInsert(lista_path);
    ///             }
    ///         </code>
    ///         Como podemos ver no exemplo anterior, primeiramente é feita uma <b>verificação</b> para saber de que categoria se deve <b>inserir os veiculos</b> e assim chamamos ainda <see cref="CarInsert(string)"/> para inserir os veiculos.
    ///     </value>
    /// </example>
    void Start() //Funcao usada no inicio da applicacao, tudo oque for feito aqui, faz logo que a applicacao inicia
    {
        if(Lista == true)
        {
            CarInsert(lista_path);
        }
        else if(Carros == true)
        {
            CarInsert(carros_path);
        }
        else if(Motas == true)
        {
            CarInsert(motas_path);
        }
        else if(Contrabando == true)
        {
            CarInsert(contrabando_path);
        }
    }
    /// <summary>
    ///     <b><i>Nome do Veiculo.</i></b>
    ///     <para>Função usada para ir <b><u>buscar o nome</u></b> do veiculo selecionado.</para>
    /// </summary>
    /// <param name="nome">Nome do Veiculo.</param>
    /// Esta função recebe como <b>parâmetro</b> o nome do veiculo que o utilizador selecionou e assim atribui ao objeto de jogo o mesmo.
    /// <example>
    ///     <b>Propriedades:</b>
    ///     <value>
    ///         <code>
    ///             this.gameObject.GetComponent<Selected_Item>().Item(nome);
    ///         </code>
    ///         O código acima mostra como se consegue ir <b>buscar o nome</b> de um veiculo que esteja em outra script.
    ///     </value>
    /// </example>
    public void CarDetails(string nome)
    {
        this.gameObject.GetComponent<Selected_Item>().Item(nome);
    }
    /// <summary>
    /// <b><i>Inserir Veiculo.</i></b>
    /// <para>Função utilizada para inserir um veiculo no Scrollview.</para>
    /// </summary>
    /// <param name="Path_Name">Caminho para o Diretório.</param>
    /// Esta função recebe como parâmetro o <b><u>caminho para o diretório</u></b> onde se encontram os veiculos dependendo da sua categoria.
    /// <example>
    ///     <b>Propriedades:</b>
    ///     <value>
    ///         <code>
    ///             DirectoryInfo dir_carros = new DirectoryInfo(Application.dataPath + "/Resources/" + Path_Name);
    ///             foreach (FileInfo i in IMG_Carros)
    ///         </code>
    ///         Caso existam veiculos na diretoria selecionada, <b>percorrerá todos os ficheiros</b> adicionando assim as imagens e os seus respetivos nomes ao <see cref="content"/>.
    ///     </value>
    /// </example>
    public void CarInsert(string Path_Name)
    {
        DirectoryInfo dir_carros = new DirectoryInfo(Application.dataPath + "/Resources/" + Path_Name);
        FileInfo[] IMG_Carros = dir_carros.GetFiles();
        if (IMG_Carros.Length == 0)
        {
            Debug.Log("Nao existem carros nessa diretoria");
        }
        else
        {
            foreach (FileInfo i in IMG_Carros)
            {
                if (i.Extension.ToLower() == ".jpg" || i.Extension.ToLower() == ".png")
                {
                    //Debug.Log(lista_path.ToString() + "/" + i.Name);
                    var currentSprite = Resources.Load<Sprite>(Path_Name + "/" + i.Name.Substring(0, i.Name.Length - 4));
                    GameObject NewObj = new GameObject(); //Cria um novo GameObject na Scene
                    Image NewImage = NewObj.AddComponent<Image>(); //Adiciona o componente Image ao NewObj
                    NewImage.sprite = currentSprite; //Atribui uma imagem ao NewImage
                    NewObj.name = i.Name.Substring(0, i.Name.Length - 4); //Altera o nome do GameObject para o mesmo que o sprite tiver
                    NewObj.GetComponent<RectTransform>().SetParent(content.transform); //Faz com que o NewObj seja "Filho" do content
                    NewObj.GetComponent<RectTransform>().localScale = new Vector3(5.0f, 5.0f, 0); //Altera a escala do NewObj
                    NewObj.SetActive(true); //Faz com que o NewObj fique visivel na Scene

                    NewObj.AddComponent<Button>();
                    Button Butao_Config = NewObj.GetComponent<Button>();
                    Butao_Config.onClick.AddListener(delegate { CarDetails(currentSprite.name.ToString()); });

                    GameObject text = new GameObject(); //Cria um GameObject com o nome text
                    Text NewText = text.AddComponent<Text>(); //Cria uma variavel do tipo Text
                    text.name = "Text"; //Muda o nome do text para Texto
                    text.GetComponent<RectTransform>().SetParent(NewObj.transform); //Faz com que o text seja "Filho" do NewObj
                    text.GetComponent<RectTransform>().localScale = new Vector3(0.1f, 0.1f, 0.1f); //Serve para mudar a escala do text
                    text.GetComponent<RectTransform>().sizeDelta = new Vector2(999f, 352f); //Muda o tamanho do text
                    text.GetComponent<RectTransform>().localPosition = new Vector3(0f, -63.8f, 0f); //Altera a posicao do text
                    Text modify_Text = text.GetComponent<Text>(); //Cria uma variavel com o componente do text de forma a alteracoes mais facilmente
                    modify_Text.font = Roboto; //Altera a fonte do text
                    modify_Text.fontSize = 150; //Altera o tamanho da font do text
                    modify_Text.horizontalOverflow = HorizontalWrapMode.Overflow; //Modifica a propriedade horizontal do text
                    modify_Text.alignment = TextAnchor.MiddleCenter; //Modifica o alinhamento do text
                    text.GetComponent<Text>().text = NewObj.name.ToString(); //Altera o nome do text durante o correr da app para o currentsprite.name
                    text.AddComponent<Outline>(); //Adiciona o componente de outline ao text
                    text.GetComponent<Outline>().effectDistance = new Vector2(5f, -5f); //Modifica a propriedade do outline do text
                    text.GetComponent<Outline>().effectColor = new Color(0, 0, 0, 1); //Modifica a propriedade do outline do text
                }
            }
        }
    }
}
