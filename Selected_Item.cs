using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

/// <summary>
/// Classe <c>Selected_Item</c> � uma Classe utilizada para selecionar veiculos e ver as suas caracteristicas.
/// </summary>
public class Selected_Item : MonoBehaviour
{
    /// <summary>
    /// Canvas do Veiculo Selecionado
    /// </summary>
    public GameObject Item_Selected_Canvas;
    /// <summary>
    /// Canvas Dos Destaques mostrado no Ecra no inicio da app
    /// </summary>
    public GameObject Default_Canvas;
    /// <summary>
    /// Caminho padr�o utilizado nos ficheiros
    /// </summary>
    string caminho_Defult = "Stand/";
    /// <summary>
    /// Imagem na posi��o zero definida pelo layout do Canvas
    /// </summary>
    public Image Imagem_0;
    /// <summary>
    /// Imagem na posi��o 1 definida pelo layout do Canvas
    /// </summary>
    public Image Imagem_1;
    /// <summary>
    /// Imagem na posi��o 2 definida pelo layout do Canvas
    /// </summary>
    public Image Imagem_2;
    /// <summary>
    /// Imagem na posi��o 3 definida pelo layout do Canvas
    /// </summary>
    public Image Imagem_3;
    /// <summary>
    /// Nome do veiculo selecionado
    /// </summary>
    public Text Nome;
    /// <summary>
    /// Descri��o do veiculo selecionado
    /// </summary>
    public Text Description;
    /// <summary>
    /// Pre�o do veiculo selecionado
    /// </summary>
    public Text Preco;

    /// <summary>
    /// <b><i>Fun��o do Veiculo.</i></b>
    /// <para>Esta fun��o <b><u>verifica se o veiculo t�m o seu diret�rio</u></b> para assim poder ir buscar as suas informa��es.</para>
    /// </summary>
    /// <param name="item_name">Nome do veiculo.</param>
    /// Esta fun��o recebe como parametro o nome do veiculo.
    /// <value>
    /// <b>Propriedade:</b>
    /// <example>
    ///     <code>
    ///         bool carros = System.IO.Directory.Exists(Application.dataPath + "/Resources/" + caminho_Defult + "Carros/" + item_name.ToUpper());
    ///         bool Motas = System.IO.Directory.Exists(Application.dataPath + "/Resources/" + caminho_Defult + "Motas/" + item_name);
    ///         bool Contrabando = System.IO.Directory.Exists(Application.dataPath + "/Resources/" + caminho_Defult + "Contrabando/" + item_name);
    ///     </code>
    ///         Com este c�digo conseguimos fazer a <b>verifica��o da exist�ncia</b> da <b>dir�toria do veiculo.</b>
    ///     <para>
    ///         Caso exista uma diretoria com o nome do veiculo, � chamada a fun��o <see cref="Items_Changer(string, string)"/>.
    ///     </para>
    /// </example>
    /// </value>

    public void Item(string item_name)
    {
        Nome.text = item_name;
        bool carros = System.IO.Directory.Exists(Application.dataPath + "/Resources/" + caminho_Defult + "Carros/" + item_name.ToUpper());
        bool Motas = System.IO.Directory.Exists(Application.dataPath + "/Resources/" + caminho_Defult + "Motas/" + item_name);
        bool Contrabando = System.IO.Directory.Exists(Application.dataPath + "/Resources/" + caminho_Defult + "Contrabando/" + item_name);
        Default_Canvas.SetActive(false);
        Item_Selected_Canvas.SetActive(true);
        if (carros)
        {
            Items_Changer(item_name.ToUpper(), "Carros/");
        }
        else if (carros == false && Motas && Contrabando == false)
        {
            Items_Changer(item_name, "Motas/");
        }
        else if (carros == false && Motas == false && Contrabando)
        {
            Items_Changer(item_name, "Contrabando/");
        }
        else
        {
            Debug.Log("Diretorio inexistente");
        }
    }
    /// <summary>
    /// <b><i>Troca de Veiculos.</b></i>
    /// <para>Esta fun��o serve para fazer a troca de imagens, descri��es e pre�o no Canvas de Veiculos Selecionados.</para>
    /// </summary>
    /// <param name="item_name">Name of the item.</param>
    /// <param name="Folder">The folder.</param>
    /// Esta fun��o recebe 2 parametros, o nome do Veiculo e o caminho para a dir�toria.
    /// <value>
    ///     <b>Propriedade:</b>
    ///     <code>
    ///         Description.text = System.IO.File.ReadAllText(Application.dataPath + "/Resources/" + caminho_Defult + Folder + item_name + "/Description.txt");
    ///         Description.text = Description.text.Remove(Description.text.LastIndexOf(System.Environment.NewLine));
    ///     </code>
    ///     As linhas de c�digo acima servem para ler a descri��o do veiculo e remove a �ltima linha relativa ao pre�o.
    /// </value>
    public void Items_Changer(string item_name, string Folder)
    {
        Description.text = System.IO.File.ReadAllText(Application.dataPath + "/Resources/" + caminho_Defult + Folder + item_name + "/Description.txt");
        Description.text = Description.text.Remove(Description.text.LastIndexOf(System.Environment.NewLine));
        string file_Location = Application.dataPath + "/Resources/" + caminho_Defult + Folder + item_name + "/Description.txt";
        string[] ALLLINES = System.IO.File.ReadAllLines(file_Location);
        long count = 0;
        using (StreamReader r = new StreamReader(file_Location))
        {
            string line;
            while ((line = r.ReadLine()) != null)
            {
                count++;
            }
        }
        Preco.text = ALLLINES[count - 1] + " �";
        DirectoryInfo dir_Items = new DirectoryInfo(Application.dataPath + "/Resources/" + caminho_Defult + Folder + item_name);
        FileInfo[] All_Items = dir_Items.GetFiles();
        if (All_Items.Length == 0)
        {
            Debug.Log("Nao existem carros nessa diretoria");
        }
        else
        {
            foreach (FileInfo i in All_Items)
            {
                if (i.Extension.ToLower() == ".png" || i.Extension.ToLower() == ".jpg")
                {
                    bool png_Check_3 = System.IO.File.Exists(Application.dataPath + "/Resources/" + caminho_Defult + Folder + item_name + "/" + "3.png");
                    bool jpg_Check_3 = System.IO.File.Exists(Application.dataPath + "/Resources/" + caminho_Defult + Folder + item_name + "/" + "3.jpg");

                    bool png_Check_2 = System.IO.File.Exists(Application.dataPath + "/Resources/" + caminho_Defult + Folder + item_name + "/" + "2.png");
                    bool jpg_Check_2 = System.IO.File.Exists(Application.dataPath + "/Resources/" + caminho_Defult + Folder + item_name + "/" + "2.jpg");
                    var _image_0 = Resources.Load<Sprite>(caminho_Defult + Folder + item_name + "/" + "1");
                    Imagem_0.sprite = _image_0;
                    Imagem_1.sprite = _image_0;


                    if (png_Check_2)
                    {
                        var _image_1 = Resources.Load<Sprite>(caminho_Defult + Folder + item_name + "/" + "2");
                        Imagem_2.sprite = _image_1;
                        Imagem_2.gameObject.SetActive(true);
                    }
                    else if (png_Check_2 == false && jpg_Check_2 == true)
                    {
                        var _image_1 = Resources.Load<Sprite>(caminho_Defult + Folder + item_name + "/" + "2");
                        Imagem_2.sprite = _image_1;
                        Imagem_2.gameObject.SetActive(true);
                    }
                    else
                    {
                        Imagem_2.gameObject.SetActive(false);
                    }

                    if (png_Check_3)
                    {
                        var _image_2 = Resources.Load<Sprite>(caminho_Defult + Folder + item_name + "/" + "3");
                        Imagem_3.sprite = _image_2;
                        Imagem_3.gameObject.SetActive(true);
                    }
                    else if (png_Check_3 == false && jpg_Check_3 == true)
                    {
                        var _image_2 = Resources.Load<Sprite>(caminho_Defult + Folder + item_name + "/" + "3");
                        Imagem_3.sprite = _image_2;
                        Imagem_3.gameObject.SetActive(true);
                    }
                    else
                    {
                        Imagem_3.gameObject.SetActive(false);
                    }
                }
            }
        }
    }
    /// <summary>
    ///     <b><i>Sele��o de Imagens.</b></i>
    ///     <para>Esta fun��o � utlizada para fazer a <b><u>troca de imagens</u></b> de quando selecionamos uma, existem 3 imagens possiveis.</para>
    /// </summary>
    /// <param name="index">Index da mini Imagem.</param>
    /// A fun��o recebe um index para saber qual das 3 mini imagens deve ter o seu outline ativo.
    /// <value>
    ///     <b>Propriedade:</b>
    ///     <code>
    ///         Imagem_1.gameObject.GetComponent<Outline>().enabled = true;
    ///         Imagem_2.gameObject.GetComponent<Outline>().enabled = false;
    ///         Imagem_3.gameObject.GetComponent<Outline>().enabled = false;
    ///         Imagem_3.gameObject.GetComponent<Outline>().enabled = false;
    ///         Imagem_0.sprite = Imagem_1.sprite;
    ///     </code>
    ///     O c�digo apresentado a cima, serve para <b><u>desativar o outline</u></b> (linha azul que fica em volta da mini imagem) de todas as imagens <b>exceto</b> da que se encontra ativa.
    /// </value>
    public void Image_Selector(int index)
    {
        if (index == 0)
        {
            Imagem_1.gameObject.GetComponent<Outline>().enabled = true;
            Imagem_2.gameObject.GetComponent<Outline>().enabled = false;
            Imagem_3.gameObject.GetComponent<Outline>().enabled = false;
            Imagem_0.sprite = Imagem_1.sprite;
        }
        else if (index == 1)
        {
            Imagem_1.gameObject.GetComponent<Outline>().enabled = false;
            Imagem_2.gameObject.GetComponent<Outline>().enabled = true;
            Imagem_3.gameObject.GetComponent<Outline>().enabled = false;
            Imagem_0.sprite = Imagem_2.sprite;
        }
        else if (index == 2)
        {
            Imagem_1.gameObject.GetComponent<Outline>().enabled = false;
            Imagem_2.gameObject.GetComponent<Outline>().enabled = false;
            Imagem_3.gameObject.GetComponent<Outline>().enabled = true;
            Imagem_0.sprite = Imagem_3.sprite;
        }
    }
    /// <summary>
    ///     <b><i>Bot�o de Voltar.</i></b>
    ///     <para>Esta fun��o � utilizada pelo <b>bot�o de voltar</b> e serve para <b>desativar o canvas do veiculo selecionado</b> e ativa o canvas dos destaques.</para>
    /// </summary>
    /// <value>
    ///     <b>Propriedade:</b>
    ///     <code>
    ///         Imagem_1.gameObject.GetComponent<Outline>().enabled = true;
    ///         Imagem_2.gameObject.GetComponent<Outline>().enabled = false;
    ///         Imagem_3.gameObject.GetComponent<Outline>().enabled = false;
    ///         Default_Canvas.SetActive(true);
    ///         Item_Selected_Canvas.SetActive(false);
    ///     <code>
    ///     Atrav�s do c�digo acima representado, � possivel <b><u>desativar todos os outlines</u></b> exceto o da imagem principal e ainda <b><u>desativamos o canvas do veiculo selecionado e ativamos o canvas dos destaques</u></b>.
    /// </value>
    public void Voltar()
    {
        Imagem_1.gameObject.GetComponent<Outline>().enabled = true;
        Imagem_2.gameObject.GetComponent<Outline>().enabled = false;
        Imagem_3.gameObject.GetComponent<Outline>().enabled = false;
        Default_Canvas.SetActive(true);
        Item_Selected_Canvas.SetActive(false);
    }
}
