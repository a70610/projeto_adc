using UnityEngine;
using System.Collections;

public class ExampleClass : MonoBehaviour
{
    /// <summary>
    ///  DocumentationClass: Is the class that will be used for explaining the 
    ///  example of automated documentation
    /// </summary> 

    ///<param name="id">value 1</enum>
    private int timer = 0; 

    public void Awake()
    {
        /// An enum type. 
        ///  The documentation block cannot be put after the enum! 

    }

    public void Start()
    {
    }
}