﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Classe <c>controlador_menu</c> é uma Classe utilizada para fazer o control dos canvas de login e destaques.
/// </summary>
public class controlador_menu : MonoBehaviour
{
    /// <summary>
    /// Canvas dos veiculos em destaque.
    /// </summary>
    public GameObject Canvas_Destaques;
    /// <summary>
    /// Canvas de inicio de sessão.
    /// </summary>
    public GameObject Canvas_Login;
    /// <summary>
    /// <b><i>Função de ativação do canvas de inicio de sessão</i></b>
    /// <para>Função usada para <b><u>ativar o canvas de login e desativar o de destaques</u></b>.</para>
    /// </summary>
    /// <example>
    ///     <b>Propriedades:</b>
    ///     <code>
    ///         Canvas_Destaques.SetActive(false);
    ///         Canvas_Login.SetActive(true);
    ///     </code>
    ///     No código acima podemos ver como <b>desativar</b> o canvas de destaque e <b>ativar</b> o canvas de login.
    /// </example>
    public void Login_Activate()
    {
        Canvas_Destaques.SetActive(false);
        Canvas_Login.SetActive(true);
    }
    /// <summary>
    /// <b><i>Função de ativação do canvas de veiculos em destaques</i></b>
    /// <para>Função usada para <b><u>ativar o canvas de veiculos em destaque e desativar o de inicio de sessão</u></b>.</para>
    /// </summary>
    /// <example>
    ///     <b>Propriedades:</b>
    ///     <code>
    ///         Canvas_Destaques.SetActive(true);
    ///         Canvas_Login.SetActive(false);
    ///     </code>
    ///     No código acima podemos ver como <b>desativar</b> o canvas de inicio de sessão e  como <b>ativar</b> o canvas de veiculos em destaque.
    /// </example>
    public void Destaques_Activate()
    {
        Canvas_Destaques.SetActive(true);
        Canvas_Login.SetActive(false);
    }
}
