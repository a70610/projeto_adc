using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Classe <c> QuitApp </c> � uma Classe utilizada para desligar a aplica��o.
/// </summary>
public class QuitApp : MonoBehaviour
{
    /// <summary>
    /// <b><i>Fun��o de Update</i></b>
    /// � uma fun��o que funciona enquanto a fun��o estiver aberta de modo a que consigamos verificar quando o utilizador pressiona a tecla para sair.
    /// </summary>
    /// <example>
    ///     <code>
    ///         if (Input.GetKeyDown(KeyCode.Escape))
    ///         {
    ///             Application.Quit();
    ///         }
    ///     </code>
    ///     No c�digo acima � feita a verifica��o no decorrer do tempo assim quando o utilizador clicar na tecla "Esc" a aplica��o fecha.
    /// </example>
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
