﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System;
/// <summary>
/// Classe <c> Users </c> é uma Classe utilizada fazer a gestão de utilizadores, criação, inicio de sessão e controlo do seu canvas de inicio.
/// </summary>
public class Users : MonoBehaviour
{
    /// <summary>
    /// Variável utilizada para verificar se a pessoa que iniciou sessão é policia.
    /// </summary>
    public static bool Police_Check;
    /// <summary>
    /// String relativa ao nome de utilizador.
    /// </summary>
    public InputField UserName;
    /// <summary>
    /// String relativa à palavra passe.
    /// </summary>
    public InputField Password;
    /// <summary>
    /// Botão utilizado na criação de conta para dizer se é policia ou não.
    /// </summary>
    public GameObject Police_check_button;
    /// <summary>
    /// Botão utilizado para fazer o login.
    /// </summary>
    public GameObject Entrar_botao;
    /// <summary>
    /// Botão usado para voltar do canvas de login ao de destaques
    /// </summary>
    public GameObject Botao_Voltar_Default;
    /// <summary>
    /// Botão usado para voltar do canvas de criação de conta ao de login.
    /// </summary>
    public GameObject Botao_Voltar_Login;
    /// <summary>
    /// Botão utilizado no canvas de criação de conta para criar a conta.
    /// </summary>
    public GameObject Botao_Criar_Conta_Final;
    /// <summary>
    /// Botão usado para ativar o canvas da criação de contas.
    /// </summary>
    public GameObject Botao_Criar_Conta;
    /// <summary>
    /// Texto usado como titulo, muda consoante o canvas que estiver ativo.
    /// </summary>
    public Text Texto_Inicio_Criar;
    /// <summary>
    /// Recebe o botão de contrabando presente no canvas das categorias.
    /// </summary>
    public GameObject Botao_Contrabando;
    /// <summary>
    /// Variável local para verificar se têm a conta iniciada.
    /// </summary>
    bool Inicio = false;
    /// <summary>
    /// <b><i>Ativador do Canvas de Criação de contas.</i></b>
    /// <para>Esta função é usada para <b><u>ativar o canvas</u></b> de criação de contas.</para>
    /// </summary>
    /// <example>
    ///     <b>Propriedades:</b>
    ///     <value>
    ///         <code>
    ///             Texto_Inicio_Criar.text = "Criar Conta";
    ///             Police_check_button.SetActive(true);
    ///             Botao_Voltar_Login.SetActive(true);
    ///         </code>
    ///         No código acima podemos ver <b>algumas das funcionalidades</b> presentes nesta função, sendo ela a <b>ativação do canvas criação de conta e a mudança do titulo</b> para "Criar Conta".
    ///     </value>
    /// </example>
    public void Create_Account_Canvas()
    {
        Texto_Inicio_Criar.text = "Criar Conta";
        Botao_Voltar_Default.SetActive(false);
        Police_check_button.SetActive(true);
        Entrar_botao.SetActive(false);
        Botao_Voltar_Login.SetActive(true);
        Botao_Criar_Conta_Final.SetActive(true);
        Botao_Criar_Conta.SetActive(false);
    }
    /// <summary>
    /// <b><i>Botão de Voltar ao Login.</i></b>
    /// <para>Esta função é usada pelo botão de voltar ao login para desativar o canvas de criação de contas e ativar o de inicio de sessão.</para>
    /// </summary>
    /// <example>
    ///     <b>Propriedades:</b>
    ///     <code>
    ///         Texto_Inicio_Criar.text = "Iniciar Sessão";
    ///         Botao_Criar_Conta.SetActive(true);
    ///         Botao_Voltar_Default.SetActive(true);
    ///     </code>
    ///     No código acima podemos ver <b>algumas das funcionalidades</b> presentes nesta função, sendo ela a <b>ativação do canvas de login e a mudança do titulo</b> para "Iniciar Sessão".
    /// </example>
    public void Voltar_Login()
    {
        Texto_Inicio_Criar.text = "Iniciar Sessão";
        Botao_Criar_Conta_Final.SetActive(false);
        Botao_Criar_Conta.SetActive(true);
        Police_check_button.SetActive(false);
        Botao_Voltar_Login.SetActive(false);
        Entrar_botao.SetActive(true);
        Botao_Voltar_Default.SetActive(true);
    }
    /// <summary>
    /// Botão de Veridifação se é policia ou não.
    /// </summary>
    bool button_on_off = false;
    /// <summary>
    /// <b><i>Função do Botão de Policia.</i></b>
    /// <para>Com o uso desta função, podemos <b><u>verificar se na criação de conta</u></b> o utilizador selecionou ou não a opção de policia.</para>
    /// </summary>
    /// <example>
    ///     <b>Propriedades:</b>
    ///     <code>
    ///         button_on_off = false;
    ///         button_on_off = true;
    ///     </code>
    /// </example>
    public void Button_On()
    {
        if(button_on_off == true)
        {
            button_on_off = false;
        }
        else
        {
            button_on_off = true;
        }
    }
    /// <summary>
    /// <b><i>Ciração de Conta.</i></b>
    /// <para>Função usada para fazer a criação de um utilizador adicionando a sua informação ao "Users.txt".</para>
    /// </summary>
    /// <example>
    ///     <b>Propriedades:</b>
    ///     <code>
    ///         var  teste = Environment.NewLine + UserName.text + " " + Password.text + " " + button_on_off;
    ///         File.AppendAllText(Application.dataPath + "/Resources/Users.txt", teste);
    ///     </code>
    ///     No código acima podemos ver como é <b><u>criada uma nova linha no ficheiro</u></b> de contas e como é <b>adicionada a informação</b> do utilizador.
    /// </example>
    public void User_Create()
    {
        var  teste = Environment.NewLine + UserName.text + " " + Password.text + " " + button_on_off;
        File.AppendAllText(Application.dataPath + "/Resources/Users.txt", teste);
    }
    /// <summary>
    /// <b><i>Inicio de Sessão.</i></b>
    /// <para>Com esta função podemos ver como é feito o login de um utilizador.</para>
    /// </summary>
    /// <example>
    ///     <b>Propriedades:</b>
    ///     <value>
    ///         <code>
    ///             try
    ///             {
    ///                 StreamReader User_File = new StreamReader(Application.dataPath + "/Resources/Users.txt");
    ///             }
    ///             catch(Exception error_code)
    ///         </code>
    ///         Inicialmente começamos por <b>b<u>verificar se o ficheiro existe</u></b>, caso exista percorrerá todo o ficheiro à procura do utilizador, caso exista algum <b>erro</b> será mostrada uma <b><u>excessão</u></b> com o erro.
    ///         <exception cref="Exception error_code"></exception>
    ///         <code>
    ///             while (!User_File.EndOfStream)
    ///             Debug.Log("Conta nao encontrada, Usernam ou Palavra Pass invalida");
    ///         </code>
    ///         O ficheiro será <b>percorrido através de um loop</b> e caso encontre a conta do utilizador, será iniciada com sucesso, caso contrário mostrará na consola uma mensagem de erro.
    ///     </value>
    /// </example>
    public void User_Verification()
    {
        try
        {
            StreamReader User_File = new StreamReader(Application.dataPath + "/Resources/Users.txt");
            while (!User_File.EndOfStream)
            {
                string linha = User_File.ReadLine();
                string username_Linha = linha.Split(' ')[0];
                string palavra_pass_Linha = linha.Split(' ')[1];
                if (UserName.text == username_Linha && Password.text == palavra_pass_Linha)
                {
                    Police_Check = bool.Parse(linha.Split(' ')[2]);
                    Inicio = true;
                    if(Police_Check == false)
                    {
                        Botao_Contrabando.SetActive(true);
                    }
                    else
                    {
                        Botao_Contrabando.SetActive(false);
                    }
                    break;
                }
                else
                {
                    continue;
                }
                // Do Something with the input. 
            }
            if (Inicio == false)
            {
                Debug.Log("Conta nao encontrada, Usernam ou Palavra Pass invalida");
            }

            User_File.Close();
        }
        catch(Exception error_code)
        {
            Debug.LogError("Erro: " + error_code);
        }
    }
}
